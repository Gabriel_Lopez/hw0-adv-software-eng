const https       = require('https');
const parseString = require('xml2js').parseString;
const port        = 80
const APPKEY      = 'app_key=44HbDXxMXjFjcC57'
const striptags = require('striptags');


let options = {
  hostname: 'api.eventful.com',
  path: '/rest/events/search?' + 'app_key=44HbDXxMXjFjcC57&category=education&location=new%20york%20city&page_size=40'
}

var req = https.request(options, function(res){
    var output = ''
    res.setEncoding('utf8')

    res.on('data', function (chunk) {
        output += chunk
    })

    res.on('end', function() {
      let rege = /(&#..;)|(&....;)/g
      let myarr;
      parseString(output, (err, result) => {
       result.search.events[0].event.forEach(function(item) {
         let newstr = item.description[0].replace(/&#39;/g, "'");
         newstr = newstr.replace(/&quot;/g, '"');
         newstr = newstr.replace(/<br\/?>/g, '\n');
         newstr = striptags(newstr);
         while((myarr = rege.exec(newstr)) != null){
           if(myarr[2] != undefined){
             console.log('Found: ' + myarr[2]);
           }
         }
         console.log(newstr);
        })
      })
    })
})

req.on('error', function(err) {
    console.log('error: ' + err.message)
})
req.end()
