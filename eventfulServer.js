const express     = require('express')
const app         = express()
const https       = require('https')
const parseString = require('xml2js').parseString
const port        = 80
const APPKEY      = 'app_key=44HbDXxMXjFjcC57'
const striptags   = require('striptags')
const mongodb     = require('mongodb')
const eventfulDB  = 'mongodb://gabeEventful:eventful@ds155490.mlab.com:55490/eventful'
const mongoose    = require('mongoose')
const ObjectId    = mongoose.Types.ObjectId
const bodyParser  = require('body-parser')
const multer      = require('multer');
const upload      = multer()


mongoose.connect(eventfulDB)
var Schema = mongoose.Schema

var eventInstanceSchema = new Schema({
  name    : String,
  events : [
    {
    eventID : String,
    note    : String
    }
  ]
})

var EventInstance = mongoose.model('eventLists', eventInstanceSchema);

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('./'))
app.listen(port)


//Get most updated category list
app.get('/getCategories', (req, res) =>{

  let options = {
    hostname: 'api.eventful.com',
    path: '/json/categories/list?' + APPKEY
  }

  var req = https.request(options, function(response){
      var output = ''
      response.setEncoding('utf8')

      response.on('data', function (chunk) {
          output += chunk
      })

      response.on('end', function() {
        console.log('categoryiue' ,typeof(output));
        output = JSON.parse(output)
        res.send(output.category)
      })
  })

  req.on('error', function(err) {
      res.send('error: ' + err.message)
  })
  req.end()

})
app.get('/search/:filters', (req, res) => {
    let urlAddOn = APPKEY + '&sort_order=popularity&include=tickets,price,popularity,links'
    let filters = req.query

    if(filters.keywords){
      urlAddOn += '&keywords=' + escape(filters.keywords)
    }

    if(filters.category){
      urlAddOn += '&category=' + escape(filters.category)
    }

    if(filters.loc){
      urlAddOn += '&location=' + escape(filters.loc)
    }

    if(filters.date){
      urlAddOn += '&date=' + filters.date
    }

    if(filters.pageNum){
      urlAddOn += '&page_number=' + filters.pageNum
    }

    if(filters.page_size){
      urlAddOn += '&page_size=' + filters.page_size
    }

    if(filters.within){
      console.log(filters.within);
      urlAddOn += '&within=' + filters.within
    }

    console.log(urlAddOn)

    let options = {
      hostname: 'api.eventful.com',
      path: '/json/events/search?' + urlAddOn
    }

    var req = https.request(options, function(response){
        var output = ''
        response.setEncoding('utf8')

        response.on('data', function (chunk) {
            output += chunk
        })

        response.on('end', function() {
          output = JSON.parse(output)
          console.log(output);
          if(output.page_number <= output.page_count){
            output.events.event.forEach(function(item) {
              let newstr = item.description ? item.description.replace(/&#39;/g, "'") : ''
              newstr = newstr.replace(/&quot;/g, '"');
              newstr = newstr.replace(/<br\/?>/g, '\n');
              newstr = striptags(newstr);
              item.description = newstr;
            })
          }
          res.send(output)
        })
    })

    req.on('error', function(err) {
        res.send('error: ' + err.message)
    })
    req.end()
})
//Add a list to the DB
app.post('/addList', upload.array(), (req, res) => {
  console.log(req.body.listName);

  let newList = EventInstance({
    name    : req.body.listName,
    choices : []
  })

  newList.save(err => {
    if(err){
      res.send(err);
    }
    else{
      res.send('Success!')
    }
  })
})

mongodb.MongoClient.connect(eventfulDB, (err, db) =>{

app.get('/getLists', (req, res) => {
  db.collection('eventlists').find({}, {__v:0, choices: 0}).toArray((err, alldocs) => {
    res.send(alldocs)
  })
})

app.get('/getListInfo', (req, res) => {
  if(req.query.id){
    console.log(req.query.id);
    db.collection('eventlists').find({_id: new ObjectId(req.query.id)}, {__v:0}).toArray((err, data) => {
      res.send(data)
    })
  }
})

app.post('/addEvent', upload.array(), (req, res) => {
  req.body.list_id.forEach(id => {
    db.collection('eventlists').update(
      {_id: new ObjectId(id)},
      {$push: {events: {eventID: req.body.event_id, note: req.body.comment}}}
    )
  })
  res.send('ok!')
})

app.get('/getListEvents', (req, res) => {
  db.collection('eventlists').find({_id: ObjectId(req.query.id)}, {events: 1, _id: 0}).toArray((err, data) =>{
    console.log(data);
    if(data.length > 0){

      let eventsFromList = data[0].events
      let eventArray = []
      let counter = 0
      let entries = data[0].events

      entries.forEach(function (entry) {
        let id = entry.eventID
        let options = {
          hostname: 'api.eventful.com',
          path: '/json/events/get?' + APPKEY + '&' + 'id=' + id
        }

        let req = https.request(options, function (response) {
          let output = ''
          response.setEncoding('utf8')

          response.on('data', function (chunk) {
              output += chunk
          })

          response.on('end', function() {
            output = JSON.parse(output)
            //Fix Image inconsistancy
            output.image = output.images ? output.images.image[0] : ''
            //Fix address inconsistancy
            output.venue_address = output.address
            eventArray.push(output)
            counter ++
            if(counter == entries.length){
              eventArray.forEach(function(item) {
                let newstr = item.description ? item.description.replace(/&#39;/g, "'").replace(/&quot;/g, '"').replace(/<br\/?>/g, '/\n/') : ''
                item.description = striptags(newstr)
              })
              res.send(eventArray);
            }
          })
        })
        req.on('error', function(err) {
          console.log(err);
        })
        req.end()
      })
    }
    else res.send('Empty Result')
    })
  })

app.get('/listContainsEvent', (req, res) =>{
  db.collection('eventlists').find({
    events: { $not: {$elemMatch: { eventID: req.query.id } } }
  }).toArray( (err, notInList) =>{
    let optgroupString = ''
    if(notInList.length !== 0){
      optgroupString += '<optgroup label="Not in: ">'

      notInList.forEach((element, ind) => {
        optgroupString += '<option value="' + element._id + '">' + element.name + '</option>'
        if(ind === notInList.length -1 ){
          optgroupString += '</optgroup>'
        }
      })
    }

   db.collection('eventlists').find({
      events: {$elemMatch: { eventID: req.query.id } }
    }).toArray( (err, inList) =>{
      if(inList.length !== 0) {
        optgroupString += '<optgroup label="Already in: ">'
        inList.forEach((element, ind) => {
          optgroupString += '<option disabled="true" value="' + element._id + '">' + element.name + '</option>'
          if(ind === inList.length -1){
            optgroupString += '</optgroup>'
            console.log(optgroupString);
            res.send(optgroupString)
          }
        })
      }
      else{
        res.send(optgroupString)
      }
    })
  })
})

})
