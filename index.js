var myApp = angular.module('myApp', [])
/******rootScope Variables :  currPage -> Current Page of the event result list.
                              meta     -> Meta data of the result list. Array structured as: [total # of results, # of pages, which page you're on]
                              events   -> List of events
                              selEvent -> The event that the user selected
                              lists    -> Array with all the saved lists and their events
                              searchView -> Boolean to determine whether to display Search results or the events in an existing list
******/
$(document).ready(function(){
  $('#category_dropdown').multiselect({ oneOrMoreSelected: '*' })
  $.get('/getCategories', categoryList =>{

    $.each(categoryList, (index, cateObj) =>{
      $('#category_dropdown').append('<option value=' + cateObj.id + '>' + cateObj.name + '</option>')
    })
    $('#category_dropdown').multiselect('rebuild')
    $('#category_dropdown').multiselect('select', 'comedy')
    $('#searchButton').trigger('click')
  })

  $('.userIn').keypress( (e) =>{
    if(e.which == 13){
      $('#searchButton').trigger('click')
    }
  })

  $( "#dateTo" ).datepicker();
  $( "#dateFrom" ).datepicker();

  $('#list_dropdown').multiselect({

  })
})

myApp.controller('searchCtrl', ($scope, $rootScope) => {

  $scope.search = function(pageNum) {
    $rootScope.searchView = true
    $('#eventListDropdown').val('')
    let nanobar = new Nanobar()
    nanobar.go(70 * Math.random())
    let query = 'filters?'

    //Adds which page to retrieve from eventful
    if(pageNum > 0){
      $rootScope.currPage = pageNum
      query += 'pageNum=' + pageNum + '&'
    }
    else{
      $rootScope.currPage = 1
    }

    let categories = $('#category_dropdown').val()

    if(categories.length != 0){
      query += 'category=' + escape(categories) + '&'
    }

    if($('#dateTo').val() && $('#dateFrom').val()){
      //convert dates into eventful YYYYMMDD00-YYYYMMDD00 format
      var reformattedToDate =  $('#dateTo').val().split('/')
      var reformattedFromDate =  $('#dateFrom').val().split('/')

      query += 'date=' + reformattedToDate[2] + reformattedToDate[0] + reformattedToDate[1]  + '00' + '-' + reformattedFromDate[2] + reformattedFromDate[0] + reformattedFromDate[1] + '00' + '&'
    }

    if($('#loc').val()){
      query += 'loc=' + escape($('#loc').val()) + '&'
    }

    if($('#keywords').val()){
      query += 'keywords=' + escape($('#keywords').val()) + '&'
    }

    if($('#resultsPerPage').val() > 0){
      query += 'page_size=' + $('#resultsPerPage').val() + '&'
    }

    if($('#rad').val() > 0){
      query += 'within=' + $('#rad').val() + '&'
    }

    query = query.substring(0, query.length -1)
    //let query = 'filters?loc=' + escape($('#loc').val()) +'&keywords=' + escape($('#keywords').val()) + '&category=' + escape(categories)

  console.log("query", query);
    $.get('/search/' + query, (data) =>{
      if(data){
        console.log('result from data', data);
        $rootScope.meta = [data.total_items, data.page_number, data.page_count]
        $rootScope.events = data.events.event
        console.log('events list', data.events.event);
        nanobar.go(100)
        $scope.$apply()
      }
    })
  }

  $scope.defaultToDate = function() {
    let date = new Date()
    date = ("0" + (date.getMonth() + 1)).slice(-2) +'/'+ ("0" + date.getDate()).slice(-2) +'/'+ date.getFullYear()
    return date
  }

  $scope.defaultFromDate = function() {
    let dateNextWeek = new Date()
    dateNextWeek.setDate(dateNextWeek.getDate() + 7)
    dateNextWeek = ("0" + (dateNextWeek.getMonth() + 1)).slice(-2) +'/'+ ("0" + dateNextWeek.getDate()).slice(-2) +'/'+ dateNextWeek.getFullYear()


    return dateNextWeek
  }

  $scope.showNextButton = function() {
    if($rootScope.events && $rootScope.currPage < $rootScope.meta[2]){
      return false;
    }
    else{
      return true;
    }
  }

  $scope.showPrevButton = function() {
    if($rootScope.events && $rootScope.currPage > 1){
      return false;
    }
    else{
      return true;
    }
  }

  $scope.nextPage = function() {
    $scope.search($rootScope.currPage + 1)
  }

  $scope.prevPage = function() {
    $scope.search($rootScope.currPage - 1)
  }

  $scope.deselectCategories = function() {
    $('#category_dropdown').multiselect('deselectAll', false)
    $('#category_dropdown').multiselect('updateButtonText')
  }

  $scope.clearParameters = function() {
    $('.userIn').val('')
    $scope.deselectCategories()
  }
})

myApp.controller('eventListCtrl', ($scope, $rootScope) =>{
  $rootScope.lists = [];

  $scope.listOptionSelection = function() {
    if($scope.selEventList == 'Add New List'){
      $scope.selEventList = ''
      $('#addListModal').modal()
    }
    else if($scope.selEventList != null){
      //Get all of the events saved in this list and display
      $.get('/getListEvents?id=' + $scope.selEventList, listData => {
        console.log('list data2', listData);
        $rootScope.events = listData
        console.log('list data', listData.length);
        $rootScope.meta = [listData.length, 0]
        $rootScope.searchView = false;
        $scope.$apply()
      })
    }
  }

  $scope.addList = function () {
    name = $('#newListName').val()
    $.post('/addList', {listName: name}, function(response) {
      console.log(response);
      $('#addListModal').modal('hide')
      $scope.getLists()
    })
  }

  $scope.getLists = function () {
    $.get('/getLists', lists =>{
      $scope.$apply(() =>{
        $rootScope.lists = lists

      })
    })
  }

  $scope.addEventtoDB = function (list_id, comment) {
    console.log('selevent', $rootScope.selEvent)
    $.post('/addEvent', {list_id: list_id, event_id: $rootScope.selEvent.id, comment: comment}, function (response) {
      console.log(response);
      $('#addEventModal').modal('hide')
    })

  }
  $scope.getLists();

})

myApp.controller('resultCtrl', ($scope, $rootScope) => {

  $scope.convertTime = function(time) {
    let tDate = new Date(time)
    if(isNaN(tDate.getTime()) || time === null){
      return 'Not Available'
    }
    let longDate = tDate.toLocaleString('en-us', {month:'long', day: 'numeric', year: 'numeric'}) + ' at ' + tDate.toLocaleString('en-us', {hour:'numeric', minute:'numeric'})
    return longDate
  }

  $scope.showEvent = function(ind) {
    console.log('working?', $rootScope.events[ind]);
    $rootScope.selEvent = $rootScope.events[ind]
  }

  $scope.getListName = function () {
    if($rootScope.searchView === false && $rootScope.lists.length !== 0){
      for(let x = 0; $rootScope.lists.length; x++){
        if($rootScope.lists[x]._id == $('#eventListDropdown').val()){
          return $rootScope.lists[x].name
        }
      }
    }
  }

  $scope.getCurrPage = function() {
    if($rootScope.meta && $rootScope.meta[1]){
      return 'Current Page: ' + $rootScope.meta[1]
    }
  }

  $scope.getTotalPage = function() {
    if($rootScope.meta && $rootScope.meta[2]){
      return 'Total Pages: ' + $rootScope.meta[2]
    }
  }

  $scope.truncate = function (name) {
    return name.length > 70 ? name.substring(0,70) + '...' : name
  }
  $scope.getNumberOfResults = function() {
    if($rootScope.meta && $rootScope.meta[0]){
      return 'Number of results: ' + $rootScope.meta[0]
    }
  }

  $scope.printModal = function () {
    $('#printingModal').modal()
  }

  $scope.printPage = function () {
    let objForPrinting = $('<div id="printingObj"></div>')
    let ul = $('<ul style="list-style-type: none"></ul>')

    if($('#listNameForPrinting')[0].checked){
      objForPrinting.html($scope.getListName())
      objForPrinting.append('<br><br>')
    }

    $rootScope.events.forEach(event => {
      let li = $('<li></li>')

      if($('#nameForPrinting')[0].checked){
        li.html(event.title)
        li.append('<br>')
      }
      if($('#addressForPrinting')[0].checked){
        li.append(event.venue_address)
        li.append('<br>')
      }
      if($('#startDateForPrinting')[0].checked){
        li.append('Starts: ' + $scope.convertTime(event.start_time))
        li.append('<br>')
      }
      if($('#endDateForPrinting')[0].checked){
        li.append('Ends: ' + $scope.convertTime(event.stop_time))
        li.append('<br>')
      }
      if($('#descriptionForPrinting')[0].checked){
        if(event.description.length > 200){
          li.append(event.description.substring(0, 190) + '...')
          li.append('<br>')
        }
        else li.append(event.description)
        li.append('<br>')
      }
      li.append('<br><br>')
      ul.append(li)
    })

    objForPrinting.append(ul)

    $('body').append(objForPrinting)
    $('#printingObj').print()
    $('#printingObj').remove()
  }

})

myApp.controller('eventCtrl', ($scope, $rootScope) =>{
  $scope.showImg = function() {
    if($rootScope.selEvent){
      if($rootScope.selEvent.image){
          return $rootScope.selEvent.image.medium.url
      } else return 'img_not_available.png'
    }
  }

  $scope.showAddress = function() {
    if($rootScope.selEvent){
      return 'Address: ' + $rootScope.selEvent.venue_address
    }
  }

  $scope.showName = function() {
    if($rootScope.selEvent){
      return 'Name: ' + $rootScope.selEvent.title
    }
  }


  $scope.showDescription = function() {
    if($rootScope.selEvent){
      return $rootScope.selEvent.description
    }
  }
  $scope.showTickets = function() {
    if($rootScope.selEvent){
      if($rootScope.selEvent.tickets){
        let tix = $rootScope.selEvent.tickets

        return tix ? tix.link.url : ''
      }
    }
  }
  $scope.addEvent = function () {
    $('#addEventModal').modal()

    //Populate Multiselect List of Lists
    $('#list_dropdown').find('option').remove()
    $('#list_dropdown').find('optgroup').remove()

    $.get('/listContainsEvent?id=' + $rootScope.selEvent.id, listData =>{
      console.log(listData);
      $('#list_dropdown').append(listData)
      $('#list_dropdown').multiselect('rebuild')

    })
  }
})
