const APPKEY      = 'app_key=44HbDXxMXjFjcC57'
const https       = require('https')



let options = {
  hostname: 'api.eventful.com',
  path: '/json/events/search?' + APPKEY + '&page_size=1&category=comedy'
}

var req = https.request(options, function(response){
    var output = ''
    response.setEncoding('utf8')

    response.on('data', function (chunk) {
        output += chunk
    })

    response.on('end', function() {
      console.log(output);
    })
})

req.on('error', function(err) {
    res.send('error: ' + err.message)
})
req.end()
